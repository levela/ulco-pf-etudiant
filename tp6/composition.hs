add42 :: Int -> Int
add42 x = x + 42

estPositif :: Int -> Bool
estPositif x = x > 0

plus42Positif :: Int -> Bool
plus42Positif = estPositif . add42

--plus42Positif :: Int -> Bool
--plus42Positif = (>0) . (+42)

mul2PlusUn :: Integer -> Integer
mul2PlusUn = (+1) . (*2)

mul2MoinsUn :: Integer -> Integer
mul2MoinsUn = (+(-1)) . (*2)

applyTwice :: (Integer -> Integer) -> Integer -> Integer
applyTwice f = f . f

main::IO()
main=do
    print (plus42Positif 50)
    print (mul2MoinsUn 50)
    print (mul2PlusUn 50)
    print (applyTwice mul2PlusUn 20)

