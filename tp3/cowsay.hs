cowsay :: String -> String
cowsay msg =
    "  " ++ replicate n '_' ++ "\n" ++
    "< " ++ msg ++ ">\n" ++
    "  " ++ replicate n '-' ++ "\n" ++
    "\n"++
    "    \\  ^__^  \n\
    \     \\ (oo)\\_______  \n\
    \       (__)\\        )\\/\\  \n\
    \             ||----w |  \n\
    \             ||     ||"

    where n = length msg

main :: IO ()
main = do
    putStrLn (cowsay "Hello world")