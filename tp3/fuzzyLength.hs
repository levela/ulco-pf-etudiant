fuzzyLength :: [a] -> String
fuzzyLength [] = "Empty"
fuzzyLength [_] = "One"
fuzzyLength [_, _] = "Two"
fuzzyLength _ = "Many"