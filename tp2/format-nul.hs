formatnul :: Int -> String
formatnul x = show x ++ " -> " ++ show x'
    where x' = case x of
                0 -> "nul"
                _-> "non nul"

formatnul2 :: Int -> String
formatnul2 x = show x ++ " est " ++ fmt x
    where fmt 0 = "nul"
          fmt _ = "non nul"

main :: IO()
main=do
    putStrLn(formatnul 12)
    putStrLn(formatnul 0)
    putStrLn(formatnul2 12)