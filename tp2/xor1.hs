xor1 :: Bool -> Bool -> Bool
xor1 a b = (a && not b) || (not a && b)

xorif :: Bool -> Bool -> Bool
xorif a b =
    if a == b
        then False
        else True

xorcase :: Bool -> Bool -> Bool
xorcase a b = case (a,b) of
    True -> False
    otherwise -> True

main :: IO()
main=do
    print(xor1 True True)
    print(xorif True False)
    print(xorcase True True)