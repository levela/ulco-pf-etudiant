
{-
formaterSigne :: Int -> String 
formaterSigne n =
    if n == 0
        then "nul"
    else if n>0
        then "positif"
        else "negatif"
    -}

formaterSigneGarde :: Int -> String
formaterSigneGarde n 
    | n == 0 = "nul"
    | n > 0 = "positif"
    | otherwise = "negatif"


main :: IO()
main = do
    putStrLn (formaterSigneGarde 10)