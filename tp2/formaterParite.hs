formaterParite :: Int -> String
formaterParite n =
    if even n
        then "pair"
        else "impair"

formaterPariteGarde :: Int -> String
formaterPariteGarde n
    | even n = "pair"
    | otherwise = "impair"

main :: IO()
main = do
putStrLn (formaterParite 10)