borneetFormat :: Double -> String 
borneetFormat x = show x ++ " -> " ++ show x'
    where x' = if x < 0
                then 0
                else if x > 1
                then 1
                else x
                
borneetFormatGarde :: Double -> String
borneetFormatGarde x = show x ++ " -> " ++ show x'
    where x'| x < 0 = 0
            | x > 1 = 1
            | otherwise = x
      


main :: IO()
main=do
putStrLn (borneetFormat 1.6)
putStrLn (borneetFormatGarde 0.2)