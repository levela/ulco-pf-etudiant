pairs :: Integral a => [a] -> [a]
pairs l = [x | x <- l, even x]

doubler :: Num a => [a] -> [a]
doubler l = [2 * x | x <- l]

mymap :: (a -> b) -> [a] -> [b]
mymap f list = [f x | x <- list]

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter p xs = [x | x<-xs, p x]

main::IO()
main = do
    print(pairs 35)
    