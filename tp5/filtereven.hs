filtereven1 :: [Int] -> [Int]
filtereven1 [] = []
filtereven1 (x:xs) = if even x then x:filtereven1 xs else filtereven1 xs

filterEven2 :: [Int] -> [Int]
filterEven2 xs = filter even xs

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter _ [] = []
myfilter f (x : xs) = if f x then x : myfilter f xs
                      else myfilter f xs

main::IO()
main = do
    print(filtereven1 [1..4])
    print(filterEven2 [1..4])