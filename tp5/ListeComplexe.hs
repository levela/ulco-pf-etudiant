multiple :: Int -> [Int]
multiple 0 = []
multiple n = [x|x <- [1..n], n `mod` x == 0]

combinaisons :: [a] -> [b] -> [(a, b)]
combinaisons l1 l2 = [(x, y) | x<-l1, y<-l2]

tripletsPyth :: Int -> [(Int, Int, Int)]
tripletsPyth n = [(x, y, z) | x <- [1 .. n], y <- [x .. n], z <- [1 .. n], (x * x) + (y * y) == (z * z)]

main::IO()
main = do
    print(multiple 35)
    print(combinaisons ["pomme", "poire"] [1,2,3,4])
    print(tripletsPyth 15)


