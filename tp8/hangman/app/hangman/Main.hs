import System.Random

{-drawpendu :: IO()
drawpendu = do
    let dessin_pendu = ["____|___________ "]
    let dessin_pendu = dessin_pendu ++ "   |/   \n\
                                        \  |    \n\
                                        \  |    \n\ 
                                        \  |    \n\ 
                                        \  |    \n\
                                      \____|___________ "
                                        
    let dessin_pendu = dessin_pendu ++ "    _______  \n\
                                        \  |/   \n\
                                        \  |    \n\
                                        \  |    \n\ 
                                        \  |    \n\ 
                                        \  |    \n\
                                    \____|___________ "

    --pas de temps pour finir "
    let dessin_pendu = dessin_pendu ++ " _______  \n\
                                        \|/   |    \n\
                                        \|   (_)   \n\
                                        \|        \n\
                                        \|    |   \n\
                                        \|   / \  \n\
                                    \____|___________ "
    putStrLn tail dessin_pendu
    drop 0 dessin_pendu-}


foundletter :: String -> String -> String
foundletter mot1 mot2 = do
    --let x = False
    [if elem lettre mot2 
    then lettre
    else '?' | lettre <- mot1]
    {-
    if elem lettre mot2
    then let x = True
    else drawpendu-}

pendu::String->String->IO()
pendu mot letterlist = do
    letter <- getLine
    do
        putStrLn(foundletter mot letter)
    do 
        let all_letters = letterlist ++ letter
        putStrLn(all_letters)
        pendu mot all_letters


main :: IO ()
main = do
    let liste_mots = ["function", "functional programming", "haskell", "list", "pattern matching", "recursion", "tuple", "type system"]
    rand <- randomRIO (0,length liste_mots - 1)
    putStrLn (liste_mots !! rand)
    pendu (liste_mots !! rand) ""

    
    