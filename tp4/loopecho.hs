loopEcho :: Int -> IO String
loopEcho 0 = return "loop terminate"
loopEcho n = do
    putStrLn ">>"
    line <- getLine
    if null line
    then return "ligne vide"
    else do
        putStrLn line
        loopEcho (n-1)
    

main::IO()
main=do
    res <- loopEcho 5
    putStrLn res


